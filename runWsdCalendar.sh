#!/bin/sh
echo "Starting with mvn install"
bash -c "mvn install"
echo "Starting jade main config"
bash -c "nohup mvn -Pjade-main exec:java >>mvn-main.log 2>/dev/null &" 2>&1 >/dev/null
bash -c "sleep 10"
echo "Starting jade agents config"
bash -c "nohup mvn -Pjade-agent exec:java >>mvn-agent.log 2>/dev/null &" 2>&1 >/dev/null

