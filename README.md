# Wieloagentowy system decyzyjny realizujący zadania kalendarza

## Repo

Repo contains lib/jade.rar here, but it should not be used directly.
To be deleted if everything works OK without it.

## Run

To download packages

    mvn install

To run application use scripts, which create GUI with jade and then runs the agents
defined in jade-agent-container.properties file

    Unix
    ./runWsdCalendar.sh
    Windows
    ./runWsdCalendar.bat
  
## Adding agents

Add agents to following directory
       
       /src/main/java/pw/elka/wsd/agents/
       
with their own behaviour in

       /src/main/java/pw/elka/agents/agentName/behaviours/
