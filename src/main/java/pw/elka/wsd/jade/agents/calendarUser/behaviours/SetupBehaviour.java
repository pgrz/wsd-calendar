package pw.elka.wsd.jade.agents.calendarUser.behaviours;

import jade.core.Agent;
import jade.core.behaviours.SimpleBehaviour;
import jade.domain.DFService;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAException;

public class SetupBehaviour extends SimpleBehaviour {

    private boolean finished = false;

    public SetupBehaviour(Agent a) {
        super(a);

    }


    public void action() {

        System.out.println(myAgent.getLocalName() + ": Dzień dobry!");

        DFAgentDescription dfd = new DFAgentDescription();
        dfd.setName(myAgent.getAID(myAgent.getLocalName()));

        try {
            DFService.register(myAgent, dfd);
            System.out.println(myAgent.getLocalName() + ": Zarejestrowałem się");
        } catch (FIPAException e) {
            System.err.println(myAgent.getLocalName() + ": Nie zarejestrowałem się i nie wiem dlaczego :(");
            e.printStackTrace(System.err);
        }

        this.finished = true;
    }

    public boolean done() {
        return this.finished;
    }

}
