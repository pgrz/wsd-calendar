package pw.elka.wsd.jade.agents.calendarUser.behaviours;

import jade.core.Agent;
import jade.core.behaviours.WakerBehaviour;
import jade.lang.acl.ACLMessage;

/**
 * @author Pgrz
 *
 * Używany, by zainicjować komunikację
 */
public class InitializingBehaviour extends WakerBehaviour {

    private ACLMessage message;

    public InitializingBehaviour(Agent agent, long timeout, ACLMessage message) {
        super(agent, timeout);
        this.message = message;
    }

    protected void onWake() {
        myAgent.send(this.message);
    }
}
