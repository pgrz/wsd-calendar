package pw.elka.wsd.jade.agents.calendarUser.behaviours;

import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.SimpleBehaviour;
import jade.domain.AMSService;
import jade.domain.FIPAAgentManagement.AMSAgentDescription;
import jade.domain.FIPAAgentManagement.SearchConstraints;
import jade.domain.FIPAException;
import jade.lang.acl.ACLMessage;
import pw.elka.wsd.jade.common.Config;
import pw.elka.wsd.jade.messages.Invite;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * @author pgrz
 *
 * Rola organizatora jest złożona z mniejszych działań, w ramach których agent pozostaje w różnych stanach oczekiwania/wykonywania
 *    -- inviteToMeeting
 *    -- awaitVotesResults
 *    -- updateMeetingTerms - potencjalnie wielokrotnie
 *    -- askForTermAcceptance lub cancelMeeting
 */
public class OrganizerRole extends SimpleBehaviour {

    private boolean finished = false;

    public OrganizerRole(Agent a) {
        super(a);
    }

    public void action() {

        List<AID> peersAids = findPeersAids();

        for (AID destination : peersAids) {
            ACLMessage msg = new ACLMessage(ACLMessage.REQUEST);
            msg.setLanguage(Config.communicationLanguage); //TODO zamienić na fabrykę
            msg.setProtocol(Config.communicationProtocol);
            msg.setConversationId(getConversationId());
            Invite invitation = new Invite();
            invitation.setInvitedAgents(peersAids);
            invitation.setInvitationID("INVITATION_" + getConversationId());
            try {
                msg.setContentObject(invitation);
                msg.addReceiver(destination);

                myAgent.addBehaviour(new InitializingBehaviour(myAgent, 500, msg));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        finished = true;
    }

    public boolean done() {
        return finished;
    }

    private String getConversationId() {
        return myAgent.getLocalName() + "_" + UUID.randomUUID().toString();
    }

    private List<AID> findPeersAids() {
        AMSAgentDescription[] agentDescriptions = null;

        System.out.println(myAgent.getLocalName() + ": Szukam kolegów");
        try {
            SearchConstraints c = new SearchConstraints();
            c.setMaxResults((long) -1);
            agentDescriptions = AMSService.search( myAgent, new AMSAgentDescription (), c );
        } catch (FIPAException e) {
            e.printStackTrace();
        }

        List<AID> agentIds = new ArrayList<>();

        if (agentDescriptions != null) {
            for (AMSAgentDescription aad : agentDescriptions) {
                if (!myAgent.getAID().equals(aad.getName())) {
                    agentIds.add(aad.getName());
                }
            }
        }
        System.out.println(myAgent.getLocalName() + ": Znalazłem " + agentIds.size() + " kolegów!");
        return agentIds;

    }

    /*@Override
    protected void scheduleFirst() {
    }

    @Override
    protected void scheduleNext(boolean b, int i) {
    }

    @Override
    protected boolean checkTermination(boolean b, int i) {
        System.out.println("terminating organizer");
        return true;
    }

    @Override
    protected Behaviour getCurrent() {
        return null;
    }

    @Override
    public Collection getChildren() {
        return null;
    }*/
}
