package pw.elka.wsd.jade.agents.calendarUser.behaviours;

import jade.core.Agent;
import jade.core.behaviours.CyclicBehaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.UnreadableException;
import pw.elka.wsd.jade.common.Config;
import pw.elka.wsd.jade.messages.Invite;

public class MessageReceiverBehaviour extends CyclicBehaviour{

    public MessageReceiverBehaviour(Agent a) {
        super(a);
    }

    public void action() {
        ACLMessage msg = myAgent.receive(Config.defaultMessageTemplate);

        if (msg == null) {
            block();
            return;
        }

        try {
            Object content = msg.getContentObject();
            if (content instanceof Invite) {
                myAgent.addBehaviour(new MemberRole(myAgent, msg));
            }
        } catch (UnreadableException e) {
            String messageContent = msg.getContent();
            System.out.println(myAgent.getLocalName() + ": New message!\n\tfrom:\t" + msg.getSender().getLocalName()
                + "\n\tw rozmowie numer: \t" + msg.getConversationId()
                + "\n\to treści:\t" + msg.getContent());

            if (messageContent != null) {
                //TODO handle messages and initiate behaviours
                switch (messageContent) {
                    case "INIT_BROKER":
                        myAgent.addBehaviour(new BrokerRole(myAgent));
                        break;
                    default:
                        break;
                }
            }
        }
    }
}
