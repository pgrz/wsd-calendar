package pw.elka.wsd.jade.agents.calendarUser.behaviours;

import jade.core.Agent;
import jade.core.behaviours.OneShotBehaviour;
import jade.core.behaviours.SimpleBehaviour;
import jade.lang.acl.ACLMessage;
import pw.elka.wsd.jade.messages.Invite;

/**
 * @author pgrz
 *
 * Rola członka powinna się składać z zachowań
 *    -- AwaitMeetingRequest
 *    -- ProvidePreferences
 *    -- AcceptFinalTerm lub RejectFinalTerm
 *    -- AwaitFinalTerm
 *    -- SaveMeetingTime
 *    -- CancelMeeting
 */
public class MemberRole extends SimpleBehaviour {

    private boolean finished = false;
    private ACLMessage request;

    public MemberRole(Agent a, ACLMessage request) {
        super(a);
        this.request = request;
    }

    public void action() {
        System.out.println(myAgent.getLocalName() + ": Jestem czlonkiem");
        myAgent.addBehaviour(new HandleInvitation(myAgent, this.request));
        finished = true;
    }

    public boolean done() {
        return finished;
    }

    class HandleInvitation extends OneShotBehaviour {
        private ACLMessage request;
        public HandleInvitation(Agent a, ACLMessage request) {
            super(a);
            this.request = request;
        }

        public void action() {
            try {
                Invite invitation = (Invite) this.request.getContentObject();
                ACLMessage reply = request.createReply();
                System.out.println(myAgent.getLocalName() + ": dostalem zaproszenie " + invitation.getInvitationID());
                reply.setContent("ACCEPT_" + invitation.getInvitationID());
                reply.setPerformative(ACLMessage.ACCEPT_PROPOSAL);
                myAgent.send(reply);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
