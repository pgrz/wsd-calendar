package pw.elka.wsd.jade.agents.calendarUser.behaviours;

import jade.core.Agent;
import jade.core.behaviours.SimpleBehaviour;

/**
 * @author pgrz
 *
 * Rola pośrednika powinna się składać z zachowań
 *    -- AwaitMeetingRequest
 *    -- Invite Children
 *    -- AwaitChildPreferences
 *    -- PassPreferencesToParent
 *    -- AwaitFinalTerm
 *    -- AskChildrenForTermAcceptance
 *    -- AcceptFinalTerm lub RejectFinalTerm
 *    -- SaveMeetingTime lub CancelMeeting
 *    -- PassCancelMessageToChildren
 */
public class BrokerRole extends SimpleBehaviour {

    private boolean finished = false;

    public BrokerRole(Agent a) {
        super(a);
    }

    public void action() {
        System.out.println(myAgent.getLocalName() + ": Jestem brokerem");

        finished = true;
    }

    public boolean done() {
        return finished;
    }

}
