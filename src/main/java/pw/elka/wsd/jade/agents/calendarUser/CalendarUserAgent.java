package pw.elka.wsd.jade.agents.calendarUser;

import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.CyclicBehaviour;
import jade.domain.AMSService;
import jade.domain.DFService;
import jade.domain.FIPAAgentManagement.AMSAgentDescription;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.SearchConstraints;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.domain.FIPAException;
import jade.lang.acl.ACLMessage;
import pw.elka.wsd.jade.agents.calendarUser.behaviours.*;
import pw.elka.wsd.jade.common.Config;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class CalendarUserAgent extends Agent {

    private List<AID> peersAids;
    protected void setup() {
        super.setup();

        addBehaviour(new SetupBehaviour(this));
        addBehaviour(new MessageReceiverBehaviour(this));

        this.peersAids = findPeersAids();

        if(getLocalName().equals("ag1")) {
            this.addBehaviour(new OrganizerRole(this));
            setup_ag1();
        }
    }

    private List<AID> findPeersAids() {
        AMSAgentDescription[] agentDescriptions = null;

        System.out.println(getLocalName() + ": Szukam kolegów");
        try {
            SearchConstraints c = new SearchConstraints();
            c.setMaxResults((long) -1);
            agentDescriptions = AMSService.search( this, new AMSAgentDescription (), c );
        } catch (FIPAException e) {
            e.printStackTrace();
        }

        List<AID> agentIds = new ArrayList<>();

        if (agentDescriptions != null) {
            for (AMSAgentDescription aad : agentDescriptions) {
                agentIds.add(aad.getName());
            }
        }
        System.out.println(getLocalName() + ": Znalazłem " + agentIds.size() + " kolegów!");
        return agentIds;

    }

    private String getConversationId() {
        return UUID.randomUUID().toString();
    }

    private void setup_ag1() {
        for (AID destination : peersAids) {
            ACLMessage msg = new ACLMessage(ACLMessage.REQUEST);
            msg.setLanguage(Config.communicationLanguage); //TODO zamienić na fabrykę
            msg.setProtocol(Config.communicationProtocol);
            msg.setConversationId(getConversationId());
            msg.setContent("INIT_BROKER");
            msg.addReceiver(destination);

            addBehaviour(new InitializingBehaviour(this, 500, msg));
        }
    }

    @Override
    protected void takeDown() {
        try {
            DFService.deregister(this);
        } catch (Exception ignored) {}
        super.takeDown();

        System.out.println(getLocalName() + ": Zamykam się");
    }
}
