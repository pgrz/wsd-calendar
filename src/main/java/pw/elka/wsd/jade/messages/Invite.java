package pw.elka.wsd.jade.messages;

import jade.core.AID;
import org.joda.time.DateTime;

import java.util.List;

public class Invite implements java.io.Serializable {

    private List<AID> invitedAgents;
    private String invitationID;
    private DateTime dateFrom;
    private DateTime dateTo;

    public void setInvitedAgents(List<AID> agentsList) {
        invitedAgents = agentsList;
    }

    public List<AID> getInvitedAgents() {
        return invitedAgents;
    }

    public String getInvitationID() {
        return invitationID;
    }

    public void setInvitationID(String invitationID) {
        this.invitationID = invitationID;
    }

    public DateTime getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(DateTime dateFrom) {
        this.dateFrom = dateFrom;
    }

    public DateTime getDateTo() {
        return dateTo;
    }

    public void setDateTo(DateTime dateTo) {
        this.dateTo = dateTo;
    }
}
