package pw.elka.wsd.jade.messages;

public interface IntegerMessageContentReceiver {
    void onMessage(Integer message);
}
