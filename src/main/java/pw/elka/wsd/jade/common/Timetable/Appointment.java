package pw.elka.wsd.jade.common.Timetable;

import org.joda.time.DateTime;
import org.joda.time.Duration;

import java.util.Date;

/**
 * @author pgrz
 *
 * Klasa wpisu w kalendarzu, korzysta z joda.time.DateTime
 */
public class Appointment {

    private final DateTime dateFrom;
    private final DateTime dateTo;

    /**
     * Podstawowy konstrktor klasy; kolejność dat i godzin nie jest weryfikowana!
     * Zakładam, że wpis bez sensu zostanie wyłapany później.
     *
     * @param dateFrom data i czas rozpoczęcia
     * @param dateTo   data i czas zakończenia
     */
    public Appointment(final DateTime dateFrom, final DateTime dateTo)
    {
        this.dateFrom = dateFrom;
        this.dateTo = dateTo;
    }

    public DateTime getDateFrom() {
        return dateFrom;
    }

    public DateTime getDateTo() {
        return dateTo;
    }

    public Date getDayFrom() {
        return dateFrom.toDate();
    }

    public Integer getHourFrom() {
        return dateFrom.getHourOfDay();
    }

    public Integer getQuarterFrom() {
        return dateFrom.getMinuteOfHour() / 15;
    }

    public Date getDayTo() {
        return dateTo.toDate();
    }

    public Integer getHourTo() {
        return dateTo.getHourOfDay();
    }

    public Integer getQuarterTo() {
        return dateTo.getMinuteOfHour() / 15;
    }

    public boolean collidesWith(DateTime dateFrom, DateTime dateTo) {
        return (this.dateFrom.isBefore(dateTo) && this.dateTo.isAfter(dateFrom));
    }

    public Duration getBreakDuration(DateTime dateFrom, DateTime dateTo) {
        if(this.dateFrom.isAfter(dateTo)) {
            return new Duration(dateTo, this.dateFrom);
        } else if (this.dateTo.isBefore(dateFrom)) {
            return new Duration (this.dateTo, dateFrom); //FIXME mogą być równe
        } else {
            return Duration.ZERO;
        }
    }
}
