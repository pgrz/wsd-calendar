package pw.elka.wsd.jade.common.Timetable;

/**
 * @quthor Pgrz
 *
 * Types of rules used in the Timetable class
 */
public enum TimetableRule {
    //TODO można rozwinąć o WORKHOURS albo stopniowanie preferencji
    DONOTDISTURB, PREFERREDHOURS
}
