package pw.elka.wsd.jade.common;

import jade.lang.acl.MessageTemplate;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import pw.elka.wsd.jade.common.Timetable.Appointment;
import pw.elka.wsd.jade.common.Timetable.Timetable;
import pw.elka.wsd.jade.common.Timetable.TimetableRule;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Pgrz
 *
 * Piękna klasa z konfiguracją wszystkiego
 */
public final class Config {

    /** Język komunikacji */
    public static final String communicationLanguage = "fipa-sl";

    /** Protokół komunikacji */
    public static final String communicationProtocol = "meetings-protocol";

    /** Domyślny wzorzec wiadomości */
    public static final MessageTemplate defaultMessageTemplate = MessageTemplate.and(
                                                                    MessageTemplate.MatchLanguage(communicationLanguage),
                                                                    MessageTemplate.MatchProtocol(communicationProtocol)
                                                                );

    /** Renegocjacja terminu  */
    public static final int maxNumberOfRenegotiations=3;

    /** Maksymalny czas oczekiwania na odpowiedź w czasie negocjacji (ms) */
    public static final int negotiationWaitTimeout=5000;

    /** Domyślna minimalna liczba kwadransów między spotkaniami */
    public static final int minBreak = 1;

    /** Mapa predefiniowanych terminarzy dla poszczególnych agentów */
    public static final Map<String, Timetable> timetableMap = createTimetableMap();

    /** Funkcja tworząca mapę terminarzy dla poszczególnych agentów */
    private static final Map<String,Timetable> createTimetableMap()
    {
        Map<String, Timetable> timetableMap = new HashMap<>();

        DateTimeFormatter dateFormat = DateTimeFormat.forPattern("dd-MM-yyyy HH:mm");
        DateTimeFormatter timeFormat = DateTimeFormat.forPattern("HH:mm");

        Timetable timetable = new Timetable();

        timetable.setRule(TimetableRule.PREFERREDHOURS, timeFormat.parseLocalTime("12:00"), timeFormat.parseLocalTime("13:00"));
        timetable.setRule(TimetableRule.PREFERREDHOURS, timeFormat.parseLocalTime("14:00"), timeFormat.parseLocalTime("16:00"));
        timetable.setRule(TimetableRule.DONOTDISTURB, timeFormat.parseLocalTime("08:30"), timeFormat.parseLocalTime("09:00"));
        timetable.setMinBreak(2);

        timetable.addAppointment(new Appointment(dateFormat.parseDateTime("25-01-2016 10:30"), dateFormat.parseDateTime("25-01-2016 11:00")));
        timetable.addAppointment(new Appointment(dateFormat.parseDateTime("25-01-2016 13:00"), dateFormat.parseDateTime("25-01-2016 15:00")));

        timetableMap.put("ag1", timetable);

        timetable = new Timetable();
        timetable.setRule(TimetableRule.PREFERREDHOURS, timeFormat.parseLocalTime("10:00"), timeFormat.parseLocalTime("13:00"));
        timetable.setRule(TimetableRule.DONOTDISTURB, timeFormat.parseLocalTime("13:30"), timeFormat.parseLocalTime("15:00"));
        timetable.setMinBreak(1);

        timetable.addAppointment(new Appointment(dateFormat.parseDateTime("25-01-2016 10:30"), dateFormat.parseDateTime("25-01-2016 11:00")));
        timetable.addAppointment(new Appointment(dateFormat.parseDateTime("25-01-2016 12:30"), dateFormat.parseDateTime("25-01-2016 14:00")));

        timetableMap.put("ag2", timetable);

        timetable = new Timetable();
        timetable.setRule(TimetableRule.PREFERREDHOURS, timeFormat.parseLocalTime("12:00"), timeFormat.parseLocalTime("13:00"));
        timetable.setRule(TimetableRule.DONOTDISTURB, timeFormat.parseLocalTime("11:30"), timeFormat.parseLocalTime("12:00"));
        timetable.setMinBreak(2);

        timetable.addAppointment(new Appointment(dateFormat.parseDateTime("25-01-2016 10:30"), dateFormat.parseDateTime("25-01-2016 11:30")));

        timetableMap.put("ag3", timetable);


        return Collections.unmodifiableMap(timetableMap);
    }
}
