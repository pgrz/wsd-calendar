package pw.elka.wsd.jade.common;

/**
 * @author Pgrz
 *
 * Ocena terminu, od 0 w górę
 */
public enum Rating {
    NO,
    RATHERNOT,
    MEHOK,
    SOSO,
    GREAT;

    public int getValue() {
        return ordinal();
    }

}
