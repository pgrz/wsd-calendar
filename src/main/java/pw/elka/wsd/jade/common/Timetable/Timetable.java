package pw.elka.wsd.jade.common.Timetable;

import java.util.*;

import org.joda.time.DateTime;
import org.joda.time.Duration;
import org.joda.time.LocalTime;
import pw.elka.wsd.jade.common.Config;
import pw.elka.wsd.jade.common.Rating;

/**
 * @author Pgrz
 *
 * Klasa terminarza; przechowuje informacje o terminach zapisanych w kalendarzu oraz o dodatkowych regułach.
 */
public class Timetable {

    public Timetable(){
        this.template = new Template();
        this.appointments = new ArrayList<>();
        this.setMinBreak(Config.minBreak);
    }

    private Template template;

    //TODO Dla wydajności przy dużej liczbie spotkań należałoby użyć innych struktur danych
    private List<Appointment> appointments;

    public void setMinBreak(final int minBreak) {
        //FIXME brzydkie logowanie
        if(minBreak < 0) {
            System.err.println("Warning: minimal break too low; setting to 0");
            this.template.setMinBreak(0);
        } else {
            this.template.setMinBreak(minBreak);
        }
    }

    public void setRule(final TimetableRule rule, final LocalTime from, final LocalTime to) {
        switch(rule) {
            case DONOTDISTURB:
                this.template.addDoNotDisturbHours(from, to);
                break;
            case PREFERREDHOURS:
                this.template.addPreferredHours(from, to);
                break;
            default:
                //FIXME brzydkie logowanie
                System.err.println("Warning: not yet implemented");
        }
    }

    public void addAppointment(Appointment appointment) {
        this.appointments.add(appointment);
        System.out.println("Added new appointment");
    }

    /**
     * Funkcja zwraca preferencje zgodne z terminarzem
     *
     * @param terms Lista par DateTime od i do terminów, dla jakich należy sprawdzić dostępność
     * @param quarters Liczba kwadransów, które ma trwać spotkanie
     * @return Mapowanie terminu na ocenę
     */
    public Map<Appointment, Rating> getPreferences(List<Map.Entry<DateTime, DateTime>> terms, Integer quarters) {
        Map<Appointment, Rating> preferences = new HashMap<>();

        for(Map.Entry<DateTime, DateTime> term : terms) {
            DateTime from = term.getKey();
            DateTime to   = term.getValue();

            for(DateTime dateIt = from ; dateIt.plusMinutes(quarters * 15).isAfter(to); dateIt = dateIt.plusMinutes(15)) {
                Rating rating = getRating(dateIt, dateIt.plusMinutes(quarters * 15));
                Appointment appointment = new Appointment(dateIt, dateIt.plusMinutes(quarters * 15));

                preferences.put(appointment, rating);
            }
        }

        return Collections.unmodifiableMap(preferences);
    }

    /**
     * Funkcja wylicza ocenę terminu
     *
     * Algorytm jest następujący:
     *  4 : termin preferowany w całości, spełniający regułę mininalnej przerwy, odległość od najbliższego spotkania poniżej długości zadanego spotkania
     *  3 : termin preferowany w części, odległość od najbliższego spotkania większa niż długość tego spotkania lub niespełnia reguły przerwy
     *  2 : termin niepreferowany, ale spełniający regułę minimalnej przerwy, odległość od najbliższego spotkania poniżej dlugości zadanego spotkania
     *  1 : termin niepreferowany, odleglość od najbliższego spotkania większa niż długość tego spotkania lub niespełnia reguły przerwy
     *  0 : termin w godzinach innego spotkania lub "nie przeszkadzać"
     *
     * @param dateFrom Data "od" dla której należy sprawdzić dostępność
     * @param dateTo Data "do" dla której należy sprawdzić dostępność
     * @return Ocena terminu
     */
    private Rating getRating(DateTime dateFrom, DateTime dateTo) {

        Duration termDuration = new Duration(dateFrom, dateTo);

        //FIXME zakładam, ze są jednego dnia, należałoby rozbić na do północy i od północy
        LocalTime timeFrom = dateFrom.toLocalTime();
        LocalTime timeTo   = dateTo.toLocalTime();

        if(template.checkIfDoNotDisturb(timeFrom, timeTo)){
            return Rating.NO;
        }

        Duration minBreak = this.template.getMinBreak();

        int howPreferred = template.checkIfPreferred(timeFrom, timeTo);

        Duration actualBreak = new Duration(Long.MAX_VALUE);

        for(Appointment appointment : this.appointments) {
            if(appointment.collidesWith(dateFrom, dateTo)) {
                return Rating.NO;
            }

            Duration breakDuration = appointment.getBreakDuration(dateFrom, dateTo);

            if(breakDuration.isShorterThan(actualBreak)) {
                actualBreak = breakDuration;
            }
        }

        //TODO sprawdzić kompletność tych argumentów. Może lepiej drzewko ifów?
        if (howPreferred == 2
            && (actualBreak.isLongerThan(minBreak)      || actualBreak.isEqual(minBreak))
            && (actualBreak.isShorterThan(termDuration) || actualBreak.isEqual(termDuration))) {
            return Rating.GREAT;
        }

        if (howPreferred >= 1
            && (actualBreak.isLongerThan(termDuration) || actualBreak.isShorterThan(minBreak))){
            return Rating.SOSO;
        }

        if(howPreferred == 0) {
            if ((actualBreak.isLongerThan(minBreak)      || actualBreak.isEqual(minBreak))
                && (actualBreak.isShorterThan(termDuration) || actualBreak.isEqual(termDuration))) {
                return Rating.MEHOK;
            }

            if((actualBreak.isLongerThan(termDuration) || actualBreak.isShorterThan(minBreak))){
                return Rating.RATHERNOT;
            }
        }

        //FIXME brzydkie logowanie
        System.err.println("UWAGA, nie powinienem się tu znaleźć! Próba oceny dla terminu " + dateFrom.toString() + " -- " + dateTo.toString());
        return Rating.NO;
    }

    private class Template {
        private Duration minBreak;

        private List<Map.Entry<LocalTime, LocalTime>> doNotDisturbTerms;
        private List<Map.Entry<LocalTime, LocalTime>> preferredTerms;

        public Template(){
            this.doNotDisturbTerms = new ArrayList<>();
            this.preferredTerms = new ArrayList<>();
        }

        public void setMinBreak(int minBreak) {
            this.minBreak = new Duration(minBreak * 15 * 60 * 1000);
        }

        public void addDoNotDisturbHours(final LocalTime from, final LocalTime to) {

            this.doNotDisturbTerms.add(new AbstractMap.SimpleEntry<>(from, to));
        }

        public void addPreferredHours(final LocalTime from, final LocalTime to) {
            this.preferredTerms.add(new AbstractMap.SimpleEntry<>(from, to));
        }

        public boolean checkIfDoNotDisturb(LocalTime timeFrom, LocalTime timeTo) {
            for(Map.Entry<LocalTime, LocalTime> term : doNotDisturbTerms) {
                LocalTime termStart = term.getKey();
                LocalTime termEnd = term.getValue();

                if(termStart.isBefore(timeTo) && termEnd.isAfter(timeFrom)) {
                    return true;
                }
            }

            return false;
        }

        public int checkIfPreferred(LocalTime timeFrom, LocalTime timeTo) {
            for(Map.Entry<LocalTime, LocalTime> term : preferredTerms) {
                LocalTime termStart = term.getKey();
                LocalTime termEnd = term.getValue();

                if(termStart.isBefore(timeTo) && termEnd.isAfter(timeFrom)) {
                    if (termStart.isBefore(timeFrom) && termEnd.isAfter(timeTo)) {//FIXME mogą byc przeciez rowne
                        return 2;
                    } else {
                        return 1;
                    }
                }
            }

            return 0;
        }

        public Duration getMinBreak() {
            return minBreak;
        }
    }
}
